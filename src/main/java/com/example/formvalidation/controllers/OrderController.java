package com.example.formvalidation.controllers;

import com.example.formvalidation.entities.Order;
import com.example.formvalidation.entities.Product;
import com.example.formvalidation.models.forms.OrderCreate;
import com.example.formvalidation.services.order.OrderService;
import com.example.formvalidation.services.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping(path = {"/order"})
public class OrderController {
    private final OrderService orderService;
    private final ProductService productService;

    @Autowired
    public OrderController(OrderService orderService, ProductService productService) {
        this.orderService = orderService;
        this.productService = productService;
    }

    @GetMapping(name = "Order_listAction", path = {"", "/", "/list"})
    public String listAction(Model model) {
        model.addAttribute("orders", orderService.findAll());
        return "order/list";
    }

    @GetMapping(name = "Order_detailAction", path = {"/{id:[0-9]*}", "/detail/{id:[0-9]*}"})
    public String detailAction(Model model, @PathVariable() Long id) {
        Order order = orderService.findById(id).get();
        model.addAttribute("order", order);
        return "order/detail";
    }

    @GetMapping(name = "Order_createAction", path = {"/new", "/add", "/create"})
    public String createAction(Model model) {
        List<Product> productList = productService.findAll();
        model.addAttribute("productList", productList);
        return "order/create";
    }

    @PostMapping(name= "Order_postCreateAction", path = {"/new", "/add", "/create"})
    public String createAction(Model model, @Valid OrderCreate form) {
        Order order = new Order();
        order.setOrderDate(LocalDate.now());
        order.setProducts(form.getProducts());

        orderService.insert(order);

        return "redirect:/order";
    }
}
