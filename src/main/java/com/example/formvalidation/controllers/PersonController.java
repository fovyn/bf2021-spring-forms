package com.example.formvalidation.controllers;

import com.example.formvalidation.entities.Person;
import com.example.formvalidation.models.forms.AddressCreate;
import com.example.formvalidation.models.forms.PersonCreate;
import com.example.formvalidation.services.person.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/persons")
public class PersonController {
    private final PersonService service;
    private final Logger logger = LoggerFactory.getLogger(PersonController.class);
    public PersonController(PersonService service) {
        this.service = service;
    }

    @GetMapping(path = {"", "/", "/list"})
    public String listAction(Model model) {
        List<Person> persons = service.findAll();

        model
                .addAttribute("persons", persons);
        return "persons/list";
    }

    @GetMapping(path = {"/create", "/add"})
    public String createAction(Model model) {
        return "persons/create";
    }

    @PostMapping(path = {"/create", "/add"})
    public String createAction(Model model, PersonCreate form, BindingResult result, ArrayList<AddressCreate> addressCreates) {
        if (result.hasErrors()) {
            logger.warn("ERRORS => "+ result.getAllErrors());
            model.addAttribute("errors", result.getAllErrors());

            return "persons/create";
        }
        logger.warn("VALID => "+ form);
        Person p = new Person();
        p.setUsername(form.getUsername());
        p.setPassword(form.getPassword());

        service.insert(p);
        return "redirect:/persons/list";
    }
}
