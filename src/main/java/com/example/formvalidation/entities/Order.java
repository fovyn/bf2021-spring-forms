package com.example.formvalidation.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Orders")
public class Order {
    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    @Setter
    private LocalDate orderDate;

    @Getter @Setter
    @ManyToMany(targetEntity = Product.class, cascade = {CascadeType.PERSIST})
    private List<Product> products = new ArrayList<>();
}
