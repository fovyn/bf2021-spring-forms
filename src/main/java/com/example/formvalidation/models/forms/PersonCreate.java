package com.example.formvalidation.models.forms;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@ToString
public class PersonCreate {
    @Getter
    @Setter
    @NotNull
    @Size(min = 2, max = 6)
    private String username;

    @Getter
    @Setter
    @NotNull
    @Size(min = 4, max = 8)
    private String password;

    @Getter
    @Setter
    private List<AddressCreate> addresses = new ArrayList<>();
}
