package com.example.formvalidation.models.forms;

import com.example.formvalidation.entities.Product;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class OrderCreate {
    @Getter @Setter
    private List<Product> products;
}
