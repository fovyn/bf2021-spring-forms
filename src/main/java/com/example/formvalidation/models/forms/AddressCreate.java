package com.example.formvalidation.models.forms;

import lombok.Getter;
import lombok.Setter;

public class AddressCreate {
    @Getter
    @Setter
    private String street;
    @Getter
    @Setter
    private String city;
}
