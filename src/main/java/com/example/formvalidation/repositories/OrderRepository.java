package com.example.formvalidation.repositories;

import com.example.formvalidation.entities.Order;
import com.example.formvalidation.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
}
