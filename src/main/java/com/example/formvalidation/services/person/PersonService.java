package com.example.formvalidation.services.person;

import com.example.formvalidation.entities.Person;

import java.util.List;

public interface PersonService {
    List<Person> findAll();
    Person insert(Person toInsert);
}
