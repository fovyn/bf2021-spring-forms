package com.example.formvalidation.services.product;

import com.example.formvalidation.entities.Product;

import java.util.List;

public interface ProductService{
    List<Product> findAll();
    Product insert(Product product);
}
