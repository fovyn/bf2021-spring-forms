package com.example.formvalidation.services.order;

import com.example.formvalidation.entities.Order;
import com.example.formvalidation.entities.Person;
import com.example.formvalidation.repositories.OrderRepository;
import com.example.formvalidation.repositories.PersonRepository;
import com.example.formvalidation.services.person.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {
    private final OrderRepository repository;

    @Autowired
    public OrderServiceImpl(OrderRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Order> findAll() {
        return repository.findAll();
    }

    @Override
    public Order insert(Order toInsert) {
        return repository.save(toInsert);
    }

    @Override
    public Optional<Order> findById(Long id) {
        return repository.findById(id);
    }
}
