package com.example.formvalidation.services.order;

import com.example.formvalidation.entities.Order;
import com.example.formvalidation.entities.Person;

import java.util.List;
import java.util.Optional;

public interface OrderService {
    List<Order> findAll();
    Optional<Order> findById(Long id);
    Order insert(Order toInsert);
}
